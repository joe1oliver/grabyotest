import React, { Component } from 'react';
import Poker from 'poker-hands';

import { suits, values } from "../utils";

import Layout from "./Layout";
import Deck from "./Deck";
import Player from "./Player";
import Button from "./Button";

import { Footer } from "../Styles/Styled";

class App extends Component {
	state = {
		players: [],
		deck: [],
		winner: null,
	}

	componentDidMount = () => {
		this.createDeck();
	}

	createDeck = () => {
		const deck = [];
		let deckPosition = 0;
		suits.forEach((suit, index) => {
			values.forEach((value) => {
				deck.push({
					suit,
					value,
					selected: false,
					deckPosition,
				})
				deckPosition += 1;
			})
		})
		this.setState({ deck }, () => this.addNewPlayers(2));
	}

	addNewPlayers = (amount = 1) => {
		if (this.state.players.length + amount > 6) {
			alert('You can only have up to 6 players');
		} else {
			let counter = 0;
			while (counter < amount) {
				const cards = this.addCards();
				const newPlayer = {
					name: `Player ${Object.keys(this.state.players).length + 1}`,
					cards,
				}
				const tempArray = this.state.players;
				tempArray.push(newPlayer);
				this.setState({
					players: tempArray,
				});
				counter += 1;
			}
		}
	}

	removePlayer = (index) => {
		if (this.state.players.length <= 2) {
			alert('There needs to be at least 2 players');
		} else {
			let players = [...this.state.players];
			this.unselectCards(players[index].cards)
			players.splice(index, 1);
			this.setState({
				players,
			});
		}
	}

	addCards = () => {
		const deck = this.state.deck;
		const cards = [];
		let counter = 0;
		while (counter < 5) {
			const cardIndex = Math.floor(Math.random()*52);
			if (!deck[cardIndex].selected) {
				cards.push(deck[cardIndex]);
				deck[cardIndex].selected = true;
				counter += 1;
			}
		}
		this.setState({ deck });
		return cards;
	}

	unselectCards = (cards) => {
		const deck = [...this.state.deck];
		cards.forEach((card) => {
			deck[card.deckPosition].selected = false;
		})
		this.setState({
			deck,
		})
	}

	updateName = (index, name) => {
		const players = this.state.players;
		players[index].name = name;
		this.setState({
			players,
		})
	}

	findWinner = () => {
		const hands = [];
		this.state.players.forEach((player) => {
			const hand = [];
			player.cards.forEach((card) => {
				hand.push(card.value+card.suit);
			})
			hands.push(hand.join(' '));
		})
		const winnerIndex = Poker.judgeWinner(hands);
		this.setState({
			winner: this.state.players[winnerIndex].name,
		}, () => alert(`The winner is ${this.state.winner}`))
	}
	render() {
		const players = this.state.players.map((player, index) => (
			<Player
				key={player.name}
				name={player.name}
				cards={player.cards}
				removePlayer={this.removePlayer}
				updateName={this.updateName}
				index={index}
			/>
		));
		return (
				<Layout>

					<section>
						<h1>Cards deck</h1>
						<Deck deck={this.state.deck} />
					</section>
					<section>
						<header>
							<h1>Players</h1>
						</header>
						<section>
							{ players }
						</section>
						<Footer>
							<Button icon="🙋‍♀️" onClick={() => this.addNewPlayers()} data-button="addPlayers">Add new player</Button>
							<Button icon="🏆" onClick={() => this.findWinner()}>Find the winner</Button>
						</Footer>
					</section>

				</Layout>
		);
	}
}

export default App;
