import React from 'react';
import { shallow } from 'enzyme';
import Player from './Player';
import Players from './Mock';

describe('<Player />', () => {
  let PlayerTest;
  const player=Players[0];
  const { name, cards } = player;

  beforeEach(() => {
    PlayerTest = shallow(
      <Player
        key={name}
        name={name}
        cards={cards}
        index={0}
    />
    )
  })

  it('should render 5 cards', () => {
    expect(PlayerTest.find('Card')).toHaveLength(5);
  });

  it ('should render an edit button', () => {
    expect(PlayerTest.find('[data-test="edit-button"]')).toHaveLength(1);
  })

  it ('should render a confirm button when name is being edited', () => {
    PlayerTest.setState({
      newName: 'TestName'
    });
    expect(PlayerTest.find('[data-test="confirm-button"]')).toHaveLength(1);
  })

})