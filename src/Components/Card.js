import React from "react";
import { StyledCard } from "../Styles/Styled";

const Card = ({ suit, value, selected }) => (
    <StyledCard suit={suit} value={value} selected={selected}>
        {value}
    </StyledCard>
);

export default Card;
