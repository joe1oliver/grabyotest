const Players = [
    {
        "name": "Player 1",
        "cards": [
            {
                "suit": "S",
                "value": "7",
                "selected": true,
                "deckPosition": 31
            },
            {
                "suit": "D",
                "value": "T",
                "selected": true,
                "deckPosition": 8
            },
            {
                "suit": "C",
                "value": "7",
                "selected": true,
                "deckPosition": 44
            },
            {
                "suit": "D",
                "value": "3",
                "selected": true,
                "deckPosition": 1
            },
            {
                "suit": "H",
                "value": "6",
                "selected": true,
                "deckPosition": 17
            }
        ]
    },
    {
        "name": "Player 2",
        "cards": [
            {
                "suit": "C",
                "value": "2",
                "selected": true,
                "deckPosition": 39
            },
            {
                "suit": "S",
                "value": "9",
                "selected": true,
                "deckPosition": 33
            },
            {
                "suit": "C",
                "value": "T",
                "selected": true,
                "deckPosition": 47
            },
            {
                "suit": "S",
                "value": "5",
                "selected": true,
                "deckPosition": 29
            },
            {
                "suit": "C",
                "value": "A",
                "selected": true,
                "deckPosition": 51
            }
        ]
    }
]

export default Players;