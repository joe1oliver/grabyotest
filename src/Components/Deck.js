import React from "react";
import Card from "./Card"
import { StyledDeck } from "../Styles/Styled";

const Deck = ({ deck }) => (
	<StyledDeck>
		{deck.map(card => (
			<Card key={card.suit+card.value} suit={card.suit} value={card.value} selected={card.selected}>
				{card.value}
			</Card>
		))}
	</StyledDeck>
);

export default Deck;
