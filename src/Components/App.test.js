import React from 'react';
import { shallow } from 'enzyme';
import ReactDOM from 'react-dom';
import App from './App';
import Players from './Mock';

describe('<App />', () => {
  let AppTest;

  beforeEach(() => {
    AppTest = shallow(<App />)
    window.alert = jest.fn();
  })

  it('renders without crashing', () => {
    const div = document.createElement('div');
    ReactDOM.render(<App />, div);
    ReactDOM.unmountComponentAtNode(div);
  });

  it('should render 2 players', () => {
    expect(AppTest.find('Player')).toHaveLength(2);
  });

  it('should add another player when a new player is added', () => {
    AppTest.instance().addNewPlayers();
    expect(AppTest.find('Player')).toHaveLength(3);
  });

  it('should remove a player when a player is removed', () => {
    AppTest.instance().addNewPlayers();
    AppTest.instance().removePlayer(0);
    expect(AppTest.find('Player')).toHaveLength(2);
  });

  it('should work out the winner correctly', () => {
    AppTest.setState({
      players: Players,
    })
    AppTest.instance().findWinner();
    expect(AppTest.state('winner')).toEqual('Player 1');
  })
})