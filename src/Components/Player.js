import React, { PureComponent } from "react";

import Button from "./Button";

import Card from "./Card";

import { PlayerHand, StyledInput } from "../Styles/Styled";

class Player extends PureComponent {
	state = {
		newName: null,
	}

	handleChange = (event) => {
		this.setState({ newName: event.target.value })
	}

	finishEdit = () => {
		this.props.updateName(this.props.index, this.state.newName)
		this.setState({ newName: null })
	}

	render() {
		let readOnly = true;
		let editButton = (
			<Button icon="✏️" onClick={() => this.setState({ newName: this.props.name })} data-test="edit-button">Edit</Button>
		)
	
		if (this.state.newName) {
			readOnly = false;
			editButton = (
				<Button icon="✏️" onClick={(event) =>this.finishEdit(event)} data-test="confirm-button">Confirm</Button>
			)
		}
	
		const cardsList = this.props.cards.map((card) => (
			<Card key={card.suit+card.value} suit={card.suit} value={card.value} selected={card.selected}>
				{card.value}
			</Card>
		));
		return (
			<article>
				<p>
					<StyledInput
						type="text"
						value={this.state.newName || this.props.name}
						readOnly={readOnly}
						onChange={(event) => this.handleChange(event)}
					/>
					{editButton}
					<Button icon="🔥" onClick={() =>this.props.removePlayer(this.props.index)}>Remove</Button>
				</p>
				<PlayerHand>
						{ cardsList }
				</PlayerHand>
			</article>
		)
	};
};

export default Player;
